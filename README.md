libvirt-gitlab-executor-collection
==================================

This repository holds a collection of Ansible roles used to:
  * prepare a bare metal host to serve as a custom gitlab executor for
    virtualization projects and effectively to be plugged into a GitLab
    project/group as a shared runner
  * perform final touches on a libvirt VM template image to serve as a GitLab
    workload runner (scheduled by a bare metal virtualization host, see above)
  * enhance virt's Satellite instance managing the bare metal shared runners
      with more user-supplied Ansible roles

Note that this is not an official Ansible collection in the sense it can be
installed like one using standard mechanisms, e.g. `ansible-galaxy`; each and
every role here is a separate unit meant to be installed when the situation
requires it.
