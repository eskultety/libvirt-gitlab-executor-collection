libvirt-gitlab-vm-runner
========================

Installs:
    - gitlab-runner agent from the official repo
    - libguestfs tools

to what becomes GitLab VM worker node template. Also caches a Fedora 38
template image with virt-builder so that bandwidth is saved when an actual
machine is provisioned in a GitLab CI pipeline.

Example Playbook
----------------

Including an example of how to use your role (for instance, with variables passed in as parameters) is always nice for users too:

    - hosts: servers
      roles:
         - role: libvirt-gitlab-vm-runner

License
-------

BSD

Author Information
------------------

Erik Skultety <eskultet@redhat.com>
