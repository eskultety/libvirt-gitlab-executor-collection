gitlab-provisioner
==================

This role prepares a baremetal system to be plugged into a GitLab instance
(defaulting to the 'custom' executor) so that GitLab can schedule VM workload
runners using libvirt through lcitool and the libvirt-gci tool.

Example Playbook
----------------

    - hosts: all
      roles:
        - role: "gitlab-provisioner"
          vars:
            gitlab_runner_name: "foo"
            gitlab_runner_registration_token: "DEADBEEF"
            gitlab_runner_url: "https://gitlab.com"
            gitlab_runner_tags: "myrunner"
            gitlab_runner_executor: "custom"

License
-------

BSD

Author Information
------------------

Erik Skultety <eskultet@redhat.com>
