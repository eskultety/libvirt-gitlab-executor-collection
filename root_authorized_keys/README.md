root_authorized_keys
====================

User supplied Satellite (Foreman) role. When a new machine is provisioned and
this role was added to the machine's configuration, SSH keys of the machine
owner or owner group are automatically uploaded to the newly provisioned
system, so that SSH connection with a key works out of the box.

The role will be recognized automatically by Satellite/Foreman if installed to
the global Ansible role store '/etc/ansible/roles'

Role Variables
--------------

ssh_authorized_keys - list of public SSH keys to install on a provisioned server

License
-------

BSD

Author Information
------------------

Erik Skultety <eskultet@redhat.com>
